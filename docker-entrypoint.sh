#!/bin/sh
set -e
# Не сработало (
# shellcheck disable=SC2039
if [[ $RUN == "dev" ]];
then
    echo "Run dev mode"
    python manage.py migrate
    python manage.py crontab add
    python manage.py loaddata fake_data.json
    python manage.py runserver 0.0.0.0:8000
fi