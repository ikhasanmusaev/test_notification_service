from django.core.validators import RegexValidator
from django.db import models

phone_regex = RegexValidator(
    regex=r'^[+]*7[0-9]{10}$',
    message="Телефон необходимо вводить в формате: '998 [XX] [XXX XX XX]'. Допускается до 11 цифр."
)

time_zone_regex = RegexValidator(
    regex=r'^[+-][0-9]{2}:[0-9]{2}$',
    message="Например +05:00"
)


class Client(models.Model):
    """
        Клиент
    """
    name = models.CharField(max_length=127, help_text='ФИО клиента', null=True, blank=True)
    tag = models.CharField(max_length=25, help_text='произвольная метка')
    mobile_operator_code = models.CharField(max_length=15, help_text='Тег для фильтра', blank=True)
    phone_number = models.CharField(max_length=12, validators=[phone_regex])
    time_zone = models.CharField(max_length=25, validators=[time_zone_regex], blank=True, null=True)

    class Meta:
        verbose_name = 'Клиент'

    def save(self, **kwargs):
        self.phone_number = '+' + self.phone_number[-11:]
        self.mobile_operator_code = self.phone_number[2:5]
        instance = super().save(**kwargs)
        return instance
