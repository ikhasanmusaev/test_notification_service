from rest_framework import mixins
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from clients.models import Client
from clients.serializers import ClientsDetailSerializers


class ClientsCreateUpdateDestroyAPIView(mixins.CreateModelMixin,
                                        mixins.UpdateModelMixin,
                                        mixins.DestroyModelMixin,
                                        GenericViewSet
                                        ):
    queryset = Client.objects.all()
    serializer_class = ClientsDetailSerializers
