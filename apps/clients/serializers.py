from rest_framework import serializers

from clients.models import Client


class ClientsDetailSerializers(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = [
            'id',
            'name',
            'tag',
            'mobile_operator_code',
            'phone_number',
            'time_zone',
        ]
