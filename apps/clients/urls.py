from django.urls import path, include
from rest_framework.routers import DefaultRouter

from clients.views import ClientsCreateUpdateDestroyAPIView

router = DefaultRouter()
router.register('clients', ClientsCreateUpdateDestroyAPIView, 'clients')

urlpatterns = [
    path('', include(router.urls)),
]
