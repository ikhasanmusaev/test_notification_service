from django.urls import path, include
from rest_framework.routers import DefaultRouter

from mailing.views import MailingModelViewSet

router = DefaultRouter()
router.register('mailing', MailingModelViewSet, 'mailing')

urlpatterns = [
    path('', include(router.urls)),
]
