from django.core.validators import RegexValidator
from django.db import models

time_interval_regex = RegexValidator(
    regex=r'^[0-9]{1,2}-[0-9]{1,2}$',
    message="Например 10-12"
)


class Mailing(models.Model):
    """
        Рассылка
    """
    text_message = models.TextField(help_text='Текст сообщения')
    tag = models.CharField(max_length=25, help_text='Тег для фильтра')
    mobile_operator_code = models.CharField(max_length=15, help_text='Тег для фильтра')

    time_interval = models.CharField(max_length=25, help_text='Временной интервал в часах [пр. 10-12]', null=True, blank=True,
                                     validators=[time_interval_regex])
    start_time_of_mailing = models.DateTimeField(help_text='дата и время запуска рассылки')
    end_time_of_mailing = models.DateTimeField(help_text='дата и время окончания рассылки')

    class Meta:
        verbose_name = 'Рассылка'


class Mail(models.Model):
    """
        Сообщение
    """
    MAIL_STATUS = (
        (1, 'created'),
        (0, 'sent'),
        (-1, 'error'),
    )

    status = models.IntegerField(choices=MAIL_STATUS, help_text='Статус отправки', default=1)
    sent_at = models.DateTimeField(null=True)

    mailing = models.ForeignKey('Mailing', on_delete=models.SET_NULL, null=True, related_name='mail')
    client = models.ForeignKey('clients.Client', on_delete=models.SET_NULL, null=True, related_name='mail')

    class Meta:
        verbose_name = 'Сообщение'
