import datetime

import requests

from clients.models import Client
from core.celery import app
from mailing.models import Mail, Mailing

header = {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTY0NjY3NDksImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9pa2hhc2FubXVzYWV2In0.fcseTHTHL2lWCIKsA8AoaViTC1TeI9XUMbCc6hOTGyI',
    'Content-Type': 'application/json'
}
url = 'https://probe.fbrq.cloud/v1/send/@id'


def call_task_sending(mailing_object, clients):
    now = datetime.datetime.now()
    start_time = mailing_object.start_time_of_mailing
    end_time = mailing_object.end_time_of_mailing

    if mailing_object.start_time_of_mailing <= now <= mailing_object.end_time_of_mailing:
        for client in clients:
            mail = Mail.objects.create(
                mailing_id=mailing_object.id,
                client_id=client,
            )
            try:
                send_mails.delay(mail.id, client, mailing_object.text_message, mailing_object.time_interval)
            except Exception as e:
                mail.status = -1
                print(e)
    elif mailing_object.start_time_of_mailing > now:
        for client in clients:
            mail = Mail.objects.create(
                mailing_id=mailing_object.id,
                client_id=client,
            )
            try:
                send_mails.apply_async(
                    (mail.id, client, mailing_object.text_message, mailing_object.time_interval, mailing_object.end_time_of_mailing),
                    eta=mailing_object.start_time_of_mailing
                )
            except Exception as e:
                mail.status = -1
                print(e)


@app.task
def send_mails(mail_id, client_id, text_message, time_interval=None, end_time=None):
    client = Client.objects.get(id=client_id)
    client_phone = client.phone_number
    client_time_zone = client.time_zone
    mail = Mail.objects.get(id=mail_id)
    now = datetime.datetime.now()

    if client_time_zone is not None:
        time_zone = client_time_zone.split(':')
        _h = int(time_zone if time_zone[0][1] != '0' else time_zone[0][0] + time_zone[0][2])
        _m = int(time_zone[1])
        now = datetime.datetime.now() + datetime.timedelta(hours=_h, minutes=_m)

    if time_interval is not None:
        _st_time_interval, _end_time_interval = time_interval.split('-')
        if not int(_st_time_interval) <= now.hour <= int(_end_time_interval):
            mail.status = -1
            mail.save()
            return

    if end_time is not None:
        if now <= end_time:
            mail.status = -1
            mail.save()
            return

    _url = url.replace('@id', str(mail.id))
    response = requests.post(
        _url,
        json={
            "id": mail.id,
            "phone": int(client_phone[-11:]),
            "text": text_message
        },
        headers=header
    )

    if response.status_code >= 200:
        mail.status = response.json()['code']
        mail.sent_at = now
    mail.save()


def tasks_error_processing():
    now = datetime.datetime.now()
    mailings = Mailing.objects.filter(start_time_of_mailing__gte=now, end_time_of_mailing__lte=now)
    for m in mailings:
        mails = Mail.objects.filter(mailing_id=m.id, sent_at__isnull=False).exclude(status=1)
        for mail in mails:
            send_mails.delay(mail.id, mail.client_id, m.text_message, m.end_time_of_mailing)
