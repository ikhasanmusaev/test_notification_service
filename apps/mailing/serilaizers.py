from django.db.models import Count
from rest_framework import serializers

from clients.models import Client
from mailing.models import Mailing, Mail
from mailing.tasks import call_task_sending


class MailingCreateUpdateSerializers(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = [
            'id',
            'text_message',
            'tag',
            'mobile_operator_code',
            'time_interval',
            'start_time_of_mailing',
            'end_time_of_mailing',
        ]

        read_only_fields = ['id', ]

    def create(self, validated_data):
        instance = super().create(validated_data)

        clients = Client.objects.filter(
            tag=validated_data['tag'],
            mobile_operator_code=validated_data['mobile_operator_code']
        ).values_list('id', flat=True)

        call_task_sending(instance, clients)

        return instance


class MailingGETSerializers(serializers.ModelSerializer):
    statistics_of_messages = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Mailing
        fields = [
            'id',
            'text_message',
            'tag',
            'mobile_operator_code',
            'start_time_of_mailing',
            'time_interval',
            'end_time_of_mailing',
            'statistics_of_messages',
        ]

        read_only_fields = ['id', ]

    def get_statistics_of_messages(self, obj):
        all_mails = Mail.objects.filter(mailing_id=obj.id).values('status')\
            .order_by('status').annotate(count_of_sent_mails=Count('id'))
        data = []
        statuses = Mail.MAIL_STATUS

        for s in statuses:
            data.append({
                "type": s[1],
                "total": all_mails.get(status=s[0])['count_of_sent_mails'] if all_mails.filter(status=s[0]) else 0
            })

        return data
