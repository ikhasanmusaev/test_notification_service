from rest_framework import mixins
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from mailing.models import Mailing
from mailing.serilaizers import MailingCreateUpdateSerializers, MailingGETSerializers


class MailingModelViewSet(ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingCreateUpdateSerializers

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return MailingGETSerializers
        return self.serializer_class
