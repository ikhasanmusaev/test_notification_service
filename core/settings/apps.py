LOCAL_APPS = [
    'clients',
    'mailing',
]

THIRD_APPS = [
    'django_filters',
    'drf_yasg',
    'corsheaders',
    'rest_framework',
    'django_crontab',
    'rest_framework.authtoken',
    'django_celery_results',
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
     # custom apps
] + LOCAL_APPS + THIRD_APPS
