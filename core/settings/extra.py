from core.settings import BASE_DIR

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

GRAPH_MODELS = {
    'all_applications': True,
    'group_models': True,
}

CRONJOBS = [
    ('*/10 * * * *', 'apps.mailing.tasks.tasks_error_processing', '>> ' + str(BASE_DIR / 'error_processing.log')), # Обработка неотправленных рассылок. Каждый 10 минут
]
