import django.conf.locale

from core.settings import env, BASE_DIR

POSTGRES_DB_CONF = {
    'ENGINE': 'django.db.backends.postgresql',
    'NAME': env('DATABASE_NAME', default='project'),
    'USER': env('DATABASE_USER', default='postgres'),
    'PASSWORD': env('DATABASE_PASSWORD', default='1'),
    'HOST': env('DATABASE_HOST', default='localhost'),
    # 'HOST': 'local_postgres',
    'PORT': env('DATABASE_PORT', default='5432'),
    # 'PORT': 5400,
}

SQLITE_DB_CONF = {
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': BASE_DIR / 'db.sqlite3',
}

# Database
DATABASES = {
    'default': POSTGRES_DB_CONF,
    # 'default': SQLITE_DB_CONF,
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
LANGUAGE_CODE = 'en'

TIME_ZONE = 'Asia/Tashkent'

USE_I18N = True

USE_L10N = True

USE_TZ = False

LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
    ('uz', 'Uzbek'),
)

EXTRA_LANG_INFO = {
    'uz': {
        'bidi': False,  # right-to-left
        'code': 'uz',
        'name': 'Uzbek',  # name in English
        'name_local': 'Uzbek',  # locale name
    },
}

django.conf.locale.LANG_INFO.update(EXTRA_LANG_INFO)

LOCALE_PATHS = (
    BASE_DIR / 'locale',
)

MODELTRANSLATION_LANGUAGES = ('ru', 'en', 'uz')

# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = BASE_DIR / env('STATIC_PATH', default='../static/')
MEDIA_ROOT = BASE_DIR / env('MEDIA_PATH', default='../media/')
