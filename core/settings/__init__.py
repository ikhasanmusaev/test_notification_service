from core.settings.defaults import *
from core.settings.locals import *
from core.settings.apps import *
from core.settings.rest_framework import *
from core.settings.extra import *
from core.settings.celery import *
