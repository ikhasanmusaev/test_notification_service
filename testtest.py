import requests

data = {'id': 68, 'phone': 79657779853, 'text': "test test message text"}
header = {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTY0NjY3NDksImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9pa2hhc2FubXVzYWV2In0.fcseTHTHL2lWCIKsA8AoaViTC1TeI9XUMbCc6hOTGyI',
    'Content-Type': 'application/json'
}
url = 'https://probe.fbrq.cloud/v1/send/68'

response = requests.post(
    url,
    json=data,
    headers=header
)

print(response)
