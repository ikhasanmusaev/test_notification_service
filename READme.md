# To run the application:

    `docker-compose up`

## User is automatically created in the database

## After the application is up and running, you can access the application at:

    `http://localhost:8000`

### test user:

    `username: i` 

    `password: 1`

### Extra tasks numbers:
[
    3,
    5,
    9,
    11
]